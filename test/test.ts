import { expect, tap } from '@pushrocks/tapbundle';
import * as qenv from '@pushrocks/qenv';

const testQenv = new qenv.Qenv('.', './.nogit');

import * as paddle from '../ts/index';

let testPaddleAccount: paddle.PaddleAccount;

tap.test('should create a paddle account', async () => {
  testPaddleAccount = new paddle.PaddleAccount(testQenv.getEnvVarOnDemand('PADDLE_APIKEY'));
});

tap.start();
